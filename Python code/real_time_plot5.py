import matplotlib.pyplot as plt
import matplotlib.animation as animation
#!/usr/bin/env python
#
#
import time
import os
import sys
import RPi.GPIO as GPIO
from decimal import *
import atexit
import signal
import subprocess
import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtGui
import numpy as np

CONTROL_C = False


def program_exit():
    # You may do some clean-up here, but you don't have to.
    print ("\n")
    print ("Exiting application... Thnxs                                            ")
    GPIO.cleanup()
    subprocess.call('setterm -cursor on', shell=True)
    subprocess.call('spincl -ib', shell=True) 
    print (" ")       
def ctrlCHandler(*whatever):
    # Just sets the value of CONTROL_C
    global CONTROL_C
    CONTROL_C = True
                 
THREEPLACES = Decimal(10) ** -3

GPIO.setwarnings(False)

GPIO.setmode(GPIO.BCM)

subprocess.call('setterm -cursor off', shell=True)
subprocess.call('spincl -ib', shell=True) 

# read SPI data from MCP3208 chip, 8 possible adc's (0 thru 7)
def readadc(adcnum, clockpin, mosipin, misopin, cspin):
        if ((adcnum > 7) or (adcnum < 0)):
                return -1
        GPIO.output(cspin   , True)

        GPIO.output(clockpin, False)  # start clock low
        GPIO.output(cspin   , False)  # bring CS low

        commandout = adcnum
        commandout |= 0x18            # start bit + single-ended bit
        commandout <<= 3              # we only need to send 5 bits here
        for i in range(5):
                if (commandout & 0x80):
                        GPIO.output(mosipin, True )
                else:
                        GPIO.output(mosipin, False)
                commandout <<= 1
                GPIO.output(clockpin, True )
                GPIO.output(clockpin, False)

        adcout = 0

        # read in one empty bit, one null bit and 12 ADC bits
        # for the ten-bit converter, there was the original number 12
        for i in range(14):
                GPIO.output(clockpin, True )
                GPIO.output(clockpin, False)
                adcout <<= 1
                if (GPIO.input(misopin)):
                        adcout |= 0x1

        GPIO.output(cspin, True)
        
        adcout >>= 1 # first bit is 'null' so drop it
        return adcout

# change these as desired - they're the pins connected from the SPI port on the ADC to the RPi
SPICLK  = 11
SPIMISO = 9
SPIMOSI = 10
SPICS   = 8

# set up the SPI interface pins

GPIO.setup(SPIMOSI, GPIO.OUT)
GPIO.setup(SPIMISO, GPIO.IN )
GPIO.setup(SPICLK,  GPIO.OUT)
GPIO.setup(SPICS,   GPIO.OUT)


subprocess.call('clear', shell=True)

# You must check CONTROL_C in your program

# call this procedure, if control-c is pressed.
signal.signal(signal.SIGINT, ctrlCHandler)

# program_exit is called, when sys.exit is executed.
atexit.register(program_exit)

print (" ")
print ("CTRL-C to exit")
print (" ")

# -*- coding: utf-8 -*-
"""
Various methods of drawing scrolling plots.
"""

win = pg.GraphicsLayoutWidget(show=True)
win.setWindowTitle('pyqtgraph example: Scrolling Plots')



# 2) Allow data to accumulate. In these examples, the array doubles in length
#    whenever it is full.
p1 = win.addPlot(title="Channel 1 - battery current in Amperes")
p2 = win.addPlot(title="Channel 2 - battery current in Amperes")
win.nextRow()
p3 = win.addPlot(title="Channel 3 - battery voltage in Volts")
p4 = win.addPlot(title="Channel 4 - battery voltage in Volts")
win.nextRow()
p5 = win.addPlot(title="Power consumption in Watts")
p6 = win.addPlot(title="Power consumption in Watts")
# Use automatic downsampling and clipping to reduce the drawing load
p1.setDownsampling(mode='peak')
p2.setDownsampling(mode='peak')
p3.setDownsampling(mode='peak')
p4.setDownsampling(mode='peak')
p5.setDownsampling(mode='peak')
p6.setDownsampling(mode='peak')
p1.setClipToView(True)
p2.setClipToView(True)
p3.setClipToView(True)
p4.setClipToView(True)
p5.setClipToView(True)
p6.setClipToView(True)
p1.setRange(xRange=[-100, 0])
p2.setRange(xRange=[-100, 0])
p3.setRange(xRange=[-100, 0])
p4.setRange(xRange=[-100, 0])
p5.setRange(xRange=[-100, 0])
p6.setRange(xRange=[-100, 0])
p1.setRange(yRange=[0, 3])
p2.setRange(yRange=[0, 3])
p3.setRange(yRange=[50, 55])
p4.setRange(yRange=[50, 55])
p5.setRange(yRange=[0, 60])
p6.setRange(yRange=[0, 60])
p1.setLimits(xMax=0)
p2.setLimits(xMax=0)
p3.setLimits(xMax=0)
p4.setLimits(xMax=0)
p5.setLimits(xMax=0)
p6.setLimits(xMax=0)
curve1 = p1.plot()
curve2 = p2.plot()
curve3 = p3.plot()
curve4 = p4.plot()
curve5 = p5.plot()
curve6 = p6.plot()
data1 = np.empty(100)
data2 = np.empty(100)
data3 = np.empty(100)
data4 = np.empty(100)
data5 = np.empty(100)
data6 = np.empty(100)
ptr1 = 0
ptr2 = 0
ptr3 = 0
ptr4 = 0
ptr5 = 0
ptr6 = 0

def update1():
    global data1, ptr1
    data1[ptr1] = round((((((readadc(0, SPICLK, SPIMOSI, SPIMISO, SPICS))*3.4/4095)/(0.385)-2.5)/0.625)*6),2)
    ptr1 += 1
    if ptr1 >= data1.shape[0]:
        tmp = data1
        data1 = np.empty(data1.shape[0] * 2)
        data1[:tmp.shape[0]] = tmp
    curve1.setData(data1[:ptr1])
    curve1.setPos(-ptr1, 0)
def update2():
    global data2, ptr2
    data2[ptr2] = round((((((readadc(1, SPICLK, SPIMOSI, SPIMISO, SPICS))*3.4/4095)/(0.385)-2.5)/0.625)*6),2)
    ptr2 += 1
    if ptr2 >= data2.shape[0]:
        tmp = data2
        data2 = np.empty(data2.shape[0] * 2)
        data2[:tmp.shape[0]] = tmp
    curve2.setData(data2[:ptr2])
    curve2.setPos(-ptr2, 0)

def update3():
    global data3, ptr3
    data3[ptr3] = round((((readadc(2, SPICLK, SPIMOSI, SPIMISO, SPICS))*3.4/4095)*23/1.035),2)
    ptr3 += 1
    if ptr3 >= data3.shape[0]:
        tmp = data3
        data3 = np.empty(data3.shape[0] * 2)
        data3[:tmp.shape[0]] = tmp
    curve3.setData(data3[:ptr3])
    curve3.setPos(-ptr3, 0)

def update4():
    global data4, ptr4
    data4[ptr4] = round((((readadc(3, SPICLK, SPIMOSI, SPIMISO, SPICS))*3.4/4095)*23/1.035),2)
    ptr4 += 1
    if ptr4 >= data4.shape[0]:
        tmp = data4
        data4 = np.empty(data4.shape[0] * 2)
        data4[:tmp.shape[0]] = tmp
    curve4.setData(data4[:ptr4])
    curve4.setPos(-ptr4, 0)

def update5():
    global data5, ptr5
    data5[ptr5] = round((((((readadc(0, SPICLK, SPIMOSI, SPIMISO, SPICS))*3.4/4095)/(0.385)-2.5)/0.625)*6),2) * round((((readadc(2, SPICLK, SPIMOSI, SPIMISO, SPICS))*3.4/4095)*23/1.035),2)
    ptr5 += 1
    if ptr5 >= data5.shape[0]:
        tmp = data5
        data5 = np.empty(data5.shape[0] * 2)
        data5[:tmp.shape[0]] = tmp
    curve5.setData(data5[:ptr5])
    curve5.setPos(-ptr5, 0)

def update6():
    global data6, ptr6
    data6[ptr6] = round((((((readadc(1, SPICLK, SPIMOSI, SPIMISO, SPICS))*3.4/4095)/(0.385)-2.5)/0.625)*6),2) * round((((readadc(3, SPICLK, SPIMOSI, SPIMISO, SPICS))*3.4/4095)*23/1.035),2)
    ptr6 += 1
    if ptr6 >= data6.shape[0]:
        tmp = data6
        data6 = np.empty(data6.shape[0] * 2)
        data6[:tmp.shape[0]] = tmp
    curve6.setData(data6[:ptr6])
    curve6.setPos(-ptr6, 0)


# update all plots
def update():

    update1()
    update2()
    update3()
    update4()
    update5()
    update6()
    
timer = pg.QtCore.QTimer()
timer.timeout.connect(update)
timer.start(50)



## Start Qt event loop unless running in interactive mode or using pyside.
if __name__ == '__main__':
    import sys
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()




