/* mcp3208.h


 
*/
#define voie_0      0

#define voie_1      1

#define voie_2      2

#define voie_3      3

#define voie_4      4

#define voie_5      5

#define voie_6      6

#define voie_7      7

 

#define      channel0     0

#define      channel1     1

 

#define      SPI_SPEED_27 50000

#define      SPI_SPEED_33 62000

#define      SPI_SPEED_50 100000

 

 

int conversion_mcp3208(uint8_t adc, uint8_t num_voie); 

 

int conversion_mcp3208(uint8_t adc, uint8_t num_voie){

       int conversion = 0, configuration = 0;

       unsigned char spiData[3] ;

      

       configuration = (num_voie<<6)|0x0600; // configuration of the channel number, start bit and single mode (see datasheet MCP3208)

       spiData[0] = configuration>>8; // The 8 MSB configuration bits are put in spiData[0] 

       spiData[1] = configuration; // The 8 LSB configuration bits are put in spiData[1]

       spiData[2] = 0x00; // spiData[2] allows to generate the clock to collect the result of the conversion 
       wiringPiSPIDataRW (adc, spiData, 3); // sending of the three bytes via the spi

       conversion = (int)(((spiData[1]&0x0F)<<8)|spiData[2]); // masking and formatting of the conversion result (0 to FFF)

       return conversion;

}
