/*speed.c*/

 

#include <stdio.h>

#include <wiringPi.h>

#include <wiringPiSPI.h>

#include <stdint.h>

#include <fcntl.h>

#include <sys/ioctl.h>

#include <time.h>

#include <linux/spi/spidev.h>

 

#include "mcp3208.h"

#define BILLION 1000000000.0

int resultatBrut = 0;

float resultat = 0;

 

int main (void)

{
       printf("channel 1; channel 2; channel 3; channel 4; consumption; time\n");
       if(wiringPiSetupGpio() < 0)

             printf("initialisation error wiringPi\n");

      

       if(wiringPiSPISetup (channel0, SPI_SPEED_27) < 0)     //Initialisation of SPI on "channel0"  

             printf("SPI error wiringPi\n");

      

       float consumption = 0.0;
       float voltage_1;
       float voltage_2;
       float current_1;
       float current_2;
       float power;
       float interval;
       float real_time = 0.0;
       struct timespec start,end ;

       while(1){
              clock_gettime(CLOCK_REALTIME,&start);
              voltage_1 = (conversion_mcp3208(channel0, voie_2)*(3.4/4095)*23/1.035);
              voltage_2 = (conversion_mcp3208(channel0, voie_3)*(3.4/4095)*23/1.035);
              current_1 = ((conversion_mcp3208(channel0,voie_0)*3.4/(4095*0.4))-2.6)*(6/0.625) ; 
              current_2 = ((conversion_mcp3208(channel0,voie_1)*3.4/(4095*0.4))-2.6)*(6/0.625) ; 
              power = voltage_2 * current_2;
              clock_gettime(CLOCK_REALTIME,&end);
             interval = (end.tv_sec - start.tv_sec) + (end.tv_nsec - start.tv_nsec)/BILLION;
             consumption = consumption + ((power/1000)*(interval/3600));
             real_time = real_time + interval ; 
             printf("%f;%f;%f;%f;%f;%f\n", current_1, current_2, voltage_1, voltage_2, consumption, real_time);

       }

       return 0 ;
}
